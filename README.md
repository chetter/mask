# mask

## Features
- no widget internal state (avoid state duplication)
- no global state
- low abstraction
- total control over the workflow of the program
- simplicity
- small code base
- no predefined backend

## Motivations
