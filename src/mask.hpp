#pragma once

#include "msk_Vec2.hpp"
#include <stack>
#include <string>

namespace msk
{
  typedef unsigned int uint;
  typedef unsigned char uchar;

  // Forward declaration
  namespace internal
  {
    class Group;
  }

  enum GroupMode
  {
    CLIP,
    WRAP
  };

  enum WidgetType
  {
    BUTTON,
    CONTEXT,
    GROUP,
    IMAGE,
    TEXT,
    WIDGET
  };

  // Dummy templates used for documentation purposes
  using CONS = void;
  template<WidgetType, typename return_value> using GET = return_value;
  template<WidgetType> using SET = void;

  // Interface
  class Context
  {
  public:
// -- Context --------------------------------------------------------------------------------------
    SET<CONTEXT>    begin();
    SET<CONTEXT>    refresh_rate(int ms);

// -- Widget tree ----------------------------------------------------------------------------------
        /*  Widget*/
    CONS/*  |___*/Group();
    CONS/*  |     |___*/Clickable(int* state, int clicked=1, int inactive=0);
    CONS/*  |     |___*/Pushable(int* state, int pushed=1, int released=0);
    CONS/*  |     |___*/Togglable(int* state, int toggled=1, int released=0);
        /*  |     |___*/Context();
    CONS/*  |___*/Entry();
    CONS/*  |___*/Image();
    CONS/*  |___*/Text();

// -- Widget ---------------------------------------------------------------------------------------
    GET<WIDGET, int>    parent_width();
    GET<WIDGET, int>    parent_height();

    GET<WIDGET, int>      pos_x();
    GET<WIDGET, int>      pos_y();
    GET<WIDGET, Vec2i>    pos()       {return Vec2i(pos_x(), pos_y());}
    GET<WIDGET, int>      width();
    GET<WIDGET, int>      height();
    GET<WIDGET, Vec2i>    size()      {return Vec2i(width(), height());}

    SET<WIDGET>   tool_tip(const std::string& str);
    SET<WIDGET>   shortcut(const std::string& str);
    SET<WIDGET>   active(bool active);

    SET<WIDGET>   resizable();

    SET<WIDGET>   pos_x(int x);
    SET<WIDGET>   pos_y(int y);
    SET<WIDGET>   pos(int x, int y)       {pos_x(x); pos_y(y);}
    SET<WIDGET>   pos(const Vec2i p)      {pos(p.x, p.y);}
    SET<WIDGET>   width(int w);
    SET<WIDGET>   height(int h);
    SET<WIDGET>   size(int w, int h)      {width(w); height(h);}
    SET<WIDGET>   size(const Vec2i s)     {size(s.x, s.y);}

    SET<WIDGET>   pos_x(float x)           {pos_x((int)parent_width()*x);}
    SET<WIDGET>   pos_y(float y)           {pos_y((int)parent_height()*y);}
    SET<WIDGET>   pos(float x, float y)     {pos_x(x); pos_y(y);}
    SET<WIDGET>   pos(const Vec2f p)      {pos(p.x, p.y);}
    SET<WIDGET>   width(float w)           {width((int)(parent_width()*w));}
    SET<WIDGET>   height(float h)          {width((int)(parent_height()*h));}
    SET<WIDGET>   size(float w, float h)    {width(w); height(h);}
    SET<WIDGET>   size(const Vec2f s)     {size(s.x, s.y);}

// -- Group ----------------------------------------------------------------------------------------
    SET<GROUP>    end();
    SET<GROUP>    mode(GroupMode m);

// -- Text -----------------------------------------------------------------------------------------
    SET<TEXT>    content(const char* str);
    SET<TEXT>    font();
  private:
    std::stack<internal::Group*> stack;
  };
}
