#pragma once

namespace msk
{
  template<typename T>
  struct Vec2
  {
    Vec2(T x, T y): x(x), y(y) {}
    T x, y;
  };

  using Vec2i = Vec2<int>;
  using Vec2f = Vec2<float>;
}
